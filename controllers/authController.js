const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const generator = require('generate-password');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');


module.exports.register = async (req, res) => {
  const {email, password, role} = req.body;

  const user = await User.findOne({email: email});

  if (user) {
    return res.status(400).json({message: 'This email used!'});
  }

  const newUser = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await newUser.save();

  res.status(200).json({message: 'Profile created successfully'});
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;

  const user = await User.findOne({email: email});

  if (!user) {
    return res.status(400).
        json({message: `No user with email ${email} found!`});
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: 'Wrong password!'});
  }

  const jwtToken = jwt.sign({email: user.email, _id: user._id}, JWT_SECRET);

  res.status(200).json({jwtToken: jwtToken});
};

module.exports.forgotPassword = async (req, res) => {
  const {email} = req.body;

  const user = await User.findOne({email: email});

  if (!user) {
    return res.status(400).json(`No user with email ${email} found!`);
  }

  const password = generator.generate({
    length: 10,
    numbers: true,
  });

  console.log(password);

  user.password = await bcrypt.hash(password, 10);

  await user.save();

  return res.status(200).
      json({message: 'New password sent to your email address'});
};
