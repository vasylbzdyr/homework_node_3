module.exports.compareSizes = (truckSizes, loadSizes) => {
  if (truckSizes.width > loadSizes.width &&
    truckSizes.length > loadSizes.length &&
    truckSizes.height > loadSizes.height &&
    truckSizes.payload > loadSizes.payload) {
    return true;
  } else {
    return false;
  }
};
