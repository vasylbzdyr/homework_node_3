module.exports.updateLoadState = async (load) => {
  switch (load.state) {
    case 'En route to Pick Up':
      load.state = 'Arrived to Pick Up';
      break;
    case 'Arrived to Pick Up':
      load.state = 'En route to delivery';
      break;
    case 'En route to delivery':
      load.state = 'Arrived to delivery';
      break;
    default:
      load.state = 'En route to Pick Up';
  };

  await load.save();
};
