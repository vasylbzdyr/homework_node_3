module.exports.updateLoadStatus = async (load, status) => {
  await load.updateOne({status});

  await load.save();
};
