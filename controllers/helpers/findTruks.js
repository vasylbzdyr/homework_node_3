const {compareSizes} = require('./compareSizes');
const {returnTruckSize} = require('./returnTruckSize');
const {Truck} = require('../../models/truckModel');

module.exports.findTrucks = async (loadSizes) => {
  const trucks = await Truck.find({
    status: 'IS',
    assigned_to: {$ne: null},
  });

  const validTruck = trucks.find((truck) => {
    return compareSizes(returnTruckSize(truck.type), loadSizes);
  });

  return validTruck;
};
