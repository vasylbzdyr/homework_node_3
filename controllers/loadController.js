const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');
const {Load} = require('../models/loadModel');
const {loadLogger} = require('./helpers/loadLogger');
const {updateLoadState} = require('./helpers/updateLoadState');
const {updateLoadStatus} = require('./helpers/updateLoadStatus');
const {findTrucks} = require('./helpers/findTruks');

module.exports.getUserLoads = async (req, res) => {
  let {offset = 0, limit = 10} = req.query;
  limit = limit > 50 ? 50 : limit;

  let loads = [];
  const user = await User.findById(req.user._id);

  if (user.role === 'DRIVER') {
    loads = await Load.find({assigned_to: user._id}).skip(+offset).limit(limit);
  } else {
    loads = await Load.find({created_by: user._id}).skip(+offset).limit(limit);
  }

  if (loads.length === 0) {
    return res.status(400).json({
      message: `Aren't loads for ${user.email}`,
    });
  } else {
    res.status(200).json({loads});
  }
};

module.exports.addLoad = async (req, res) => {
  const {_id} = req.user;

  const {name,
    payload,
    pickup_address,
    delivery_address,
    dimensions} = req.body;

  const newLoad = new Load({
    created_by: _id,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });

  await loadLogger(newLoad, `User with ${_id} add load`);

  await newLoad.save();

  res.status(200).json({message: 'Load created successfully'});
};

module.exports.getActiveLoad = async (req, res) => {
  const {_id: id} = req.user;

  const assignTruck = await Truck.findOne({
    assigned_to: id,
  });

  if (!assignTruck) {
    return res.status(400).json({message: `No assigned trucks found`});
  }

  const load =
  await Load.findOne({assigned_to: assignTruck._id,
    status: 'ASSIGNED'}, {__v: 0});

  if (!load) {
    return res.status(400).json({message: `No active load`});
  }

  res.status(200).json({load});
};

module.exports.postLoad = async (req, res) => {
  const {id} = req.params;
  const load = await Load.findOne({_id: id, status: 'NEW'});

  if (!load) {
    return res.
        status(400).
        json({message: `Load with this id isn't or is in processing`});
  }

  await updateLoadStatus(load, 'POSTED');

  await loadLogger(load, `Load changed status from NEW to POSTED`);

  await loadLogger(load, `System start finding truck for load`);

  const truck = await findTrucks({payload: load.payload, ...load.dimensions});

  if (!truck) {
    loadLogger(load, `No assigned trucks found`);

    await updateLoadStatus(load, 'NEW');

    loadLogger(load, `Load changed status from POSTED to NEW`);

    await load.save();
    return res.status(400).json({message: 'No assigned trucks found'});
  } else {
    await loadLogger(load, `Truck found. Truck id is ${truck._id}`);

    truck.status = 'OL';
    await truck.save();

    await updateLoadStatus(load, 'ASSIGNED');

    load.assigned_to = truck._id;
    await loadLogger(load, `Load assigned to driver with id ${truck._id}`);

    await updateLoadState(load);
    await loadLogger(load,
        `Load state set '${load.state}'`);

    await load.save();
    return res.status(200).json({
      'message': 'Load posted successfully',
      'driver_found': true,
    });
  }
};

module.exports.updateNextState = async (req, res) => {
  const {_id: id} = req.user;

  const assignTruck = await Truck.findOne({
    assigned_to: id,
  });

  if (!assignTruck) {
    return res.status(400).json({
      message: `You don't have assigned truck!`,
    });
  }

  const load = await Load.findOne({
    assigned_to: assignTruck._id, status: 'ASSIGNED',
  }, {__v: 0});

  if (!load) {
    return res.status(400).json({
      message: `You don't have any load`,
    });
  }

  const oldState = load.state;
  await updateLoadState(load);
  await loadLogger(load,
      `Load state changed from ${oldState} to ${load.state}`);

  await load.save();

  if (load.state === 'Arrived to delivery') {
    await updateLoadStatus(load, 'SHIPPED');

    await loadLogger(load,
        `Load is shipped`);

    await load.save();

    const truck = await Truck.findById(load.assigned_to);

    truck.status = 'IS';
    await truck.save();
  }

  return res.status(200).json({
    message: `Load state changed to ${load.state}`,
  });
};

module.exports.getLoadById = async (req, res) => {
  const {id} = req.params;

  const load = await Load.findById(id, {__v: 0});

  if (!load) {
    return res.status(400).json({
      message: `Isn't load with id ${id}`,
    });
  }

  return res.status(200).json({load});
};

module.exports.updateLoadById = async (req, res) => {
  const {id} = req.params;

  await Load.findByIdAndUpdate(id, req.body);

  res.status(200).json({message: 'Load details changed successfully'});
};

module.exports.deleteLoadById = async (req, res) => {
  const {id} = req.params;

  const load = await Load.findById(id);

  if (!load) {
    return res.status(400).json({
      message: `Isn't load with id ${id}`,
    });
  }

  if (load.status === 'ASSIGNED') {
    return res.status(400).json({message: `Load with id ${id} in way!`});
  }

  await Load.findByIdAndDelete(id);

  return res.status(200).json({message: 'Load deleted successfully'});
};

module.exports.getUserShippingInfo = async (req, res) => {
  const {id} = req.params;

  const load = await Load.findById(id, {__v: 0});

  if (!load) {
    return res.status(400).json({
      message: `Isn't load with id ${id}`,
    });
  }

  return res.status(200).json({load});
};
