const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

module.exports.getInformation = async (req, res) => {
  const user = await User.findById(req.user._id);

  return res.status(200).json({
    _id: user._id,
    email: user.email,
    created_date: user.created_date,
  });
};

module.exports.deleteUser = async (req, res) => {
  await User.findByIdAndDelete(req.user._id);

  return res.status(200).json({
    message: 'Profile deleted successfully',
  });
};

module.exports.changePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(req.user._id);
  const match = await bcrypt.compare(oldPassword, user.password);

  if (!match) {
    return res.status(400).json({message: 'Wrong old password!'});
  }

  if (oldPassword === newPassword) {
    return res
        .status(400)
        .json({
          message: 'New password is the same ilke old password.' +
          'Please write other password.',
        });
  } else {
    user.password = await bcrypt.hash(newPassword, 10);
    await user.save();
    return res.status(200).json({message: 'Password changed successfully'});
  }
};
