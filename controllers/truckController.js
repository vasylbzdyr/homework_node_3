const {Truck} = require('../models/truckModel');

module.exports.getTrucks = async (req, res) => {
  const {email, _id} = req.user;
  const trucks = await Truck.find({created_by: _id}, {__v: 0});

  if (trucks.length === 0) {
    return res.status(400).json({message: `Aren't trucks for ${email}`});
  }

  res.status(200).json({trucks: trucks});
};

module.exports.addTruck = async (req, res) => {
  const {type} = req.body;
  const userId = req.user._id;

  const truck = new Truck({
    created_by: userId,
    type,
  });

  await truck.save();

  res.status(200).json({message: `Truck created successfully`});
};

module.exports.getTruckById = async (req, res) => {
  const {id} = req.params;
  const userId = req.user._id;
  const truck = await Truck.findOne({_id: id, created_by: userId}, {__v: 0});
  if (!truck) {
    return res.status(400).json({message: `No truck with id ${id} found!`});
  }

  res.status(200).json(truck);
};

module.exports.updateTruckById = async (req, res) => {
  const {id} = req.params;
  const {type} = req.body;

  const truck = await Truck.findById(id);

  if (!truck) {
    return res.status(400).json({message: `No truck with id ${id} found!`});
  }

  if (truck.type === type) {
    return res
        .status(400)
        .json({message: `Don't need to update the same type truck!`});
  }

  truck.type = type;
  await truck.save();

  res.status(200).json({message: `Truck details changed successfully`});
};

module.exports.deleteTruckById = async (req, res) => {
  const {id} = req.params;

  const truck = await Truck.findById(id);

  if (!truck) {
    return res.status(400).json({message: `No truck with id ${id} found!`});
  }

  await truck.remove();

  res.status(200).json({message: `Truck deleted successfully`});
};

module.exports.assignTruckById = async (req, res) => {
  const {id} = req.params;
  const userId = req.user._id;


  const truck = await Truck.findById(id);

  if (!truck) {
    return res.status(400).json({message: `No truck with id ${id} found!`});
  }

  const prevTruck = await Truck.findOne({assigned_to: userId});

  if (prevTruck) {
    prevTruck.assigned_to = null;
    await prevTruck.save();
  }

  truck.assigned_to = userId;
  await truck.save();

  return res.status(200).json({message: 'Truck assigned successfully'});
};
