const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },

  assigned_to: {
    type: String,
    default: null,
  },

  status: {
    type: String,
    required: true,
    default: 'NEW',
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
  },

  state: {
    type: String,
    default: null,
  },

  name: {
    type: String,
    required: true,
  },

  payload: {
    type: Number,
    required: true,
  },

  pickup_address: {
    type: String,
    required: true,
  },

  delivery_address: {
    type: String,
    required: true,
  },

  dimensions: {
    width: {
      type: Number,
      required: true,
    },

    length: {
      type: Number,
      required: true,
    },

    height: {
      type: Number,
      required: true,
    },
  },

  logs: [{
    message: {
      type: String,
    },
    time: {
      type: Date,
      default: Date.now(),
    },
  }],

  createDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Load = mongoose.model('Load', loadSchema);
