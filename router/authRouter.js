const express = require('express');
const router = new express.Router();

const {asyncWrapper} =
require('../middlewares/helpers');

const {validateRegister,
  validateLogin,
  validateEmail} =
    require('../middlewares/validationMiddleware');

const {register,
  login,
  forgotPassword} = require('../controllers/authController');

router.post('/register',
    asyncWrapper(validateRegister),
    asyncWrapper(register));

router.post('/login',
    asyncWrapper(validateLogin),
    asyncWrapper(login));

router.post('/forgot_password',
    asyncWrapper(validateEmail),
    asyncWrapper(forgotPassword));

module.exports = router;
