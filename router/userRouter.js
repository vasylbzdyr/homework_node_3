const express = require('express');
const router = new express.Router();

const {existingProfileMiddleware} =
  require('../middlewares/existingProfileMiddleware');
const {authLoginMiddleware,
  authShipperRoleMiddleware} = require('../middlewares/authMiddleware');
const {validatePassword} = require('../middlewares/validationMiddleware');
const {asyncWrapper} = require('../middlewares/helpers');
const {checkDriverStatus} = require('../middlewares/checkDriverStatus');

const {
  getInformation,
  deleteUser,
  changePassword,
} = require('../controllers/userController');

router.get('/', authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(getInformation));

router.delete('/', authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(authShipperRoleMiddleware),
    asyncWrapper(deleteUser));

router.patch('/password', authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(checkDriverStatus),
    asyncWrapper(validatePassword),
    asyncWrapper(changePassword));

module.exports = router;
