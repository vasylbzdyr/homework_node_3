const express = require('express');
const router = new express.Router();

const {existingProfileMiddleware} =
require('../middlewares/existingProfileMiddleware');

const {authLoginMiddleware,
  authDriverRoleMiddleware} = require('../middlewares/authMiddleware');

const {validateTruckType} = require('../middlewares/validationMiddleware');
const {asyncWrapper} = require('../middlewares/helpers');
const {
  getTrucks,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
} = require('../controllers/truckController');
const {checkDriverStatus} = require('../middlewares/checkDriverStatus');

router.get('/',
    authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(authDriverRoleMiddleware),
    asyncWrapper(getTrucks));

router.post('/',
    authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(authDriverRoleMiddleware),
    asyncWrapper(checkDriverStatus),
    asyncWrapper(validateTruckType),
    asyncWrapper(addTruck));

router.get('/:id',
    authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(authDriverRoleMiddleware),
    asyncWrapper(getTruckById));

router.put('/:id',
    authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(authDriverRoleMiddleware),
    asyncWrapper(checkDriverStatus),
    asyncWrapper(validateTruckType),
    asyncWrapper(updateTruckById));

router.delete('/:id',
    authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(authDriverRoleMiddleware),
    asyncWrapper(checkDriverStatus),
    asyncWrapper(deleteTruckById));

router.post('/:id/assign',
    authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(authDriverRoleMiddleware),
    asyncWrapper(checkDriverStatus),
    asyncWrapper(assignTruckById),
);

module.exports = router;
