const express = require('express');
const router = new express.Router();

const {asyncWrapper} =
    require('../middlewares/helpers');

const {getUserLoads,
  addLoad,
  postLoad,
  getActiveLoad,
  updateNextState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  getUserShippingInfo} = require('../controllers/loadController');

const {existingProfileMiddleware} =
require('../middlewares/existingProfileMiddleware');

const {authLoginMiddleware,
  authShipperRoleMiddleware,
  authDriverRoleMiddleware} =
  require('../middlewares/authMiddleware');

const {checkLoadStatus} = require('../middlewares/checkLoadStatus');
const {validateLoad} = require('../middlewares/validationMiddleware');


router.get('/', authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(getUserLoads));

router.post('/', authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(authShipperRoleMiddleware),
    asyncWrapper(addLoad));

router.post('/:id/post',
    authLoginMiddleware,
    asyncWrapper(authShipperRoleMiddleware),
    asyncWrapper(checkLoadStatus),
    asyncWrapper(postLoad));

router.get('/active',
    authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(authDriverRoleMiddleware),
    asyncWrapper(getActiveLoad));

router.patch('/active/state',
    authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(authDriverRoleMiddleware),
    asyncWrapper(updateNextState),
);

router.get('/:id',
    authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(getLoadById),
);

router.put('/:id', authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(authShipperRoleMiddleware),
    asyncWrapper(checkLoadStatus),
    asyncWrapper(validateLoad),
    asyncWrapper(updateLoadById),
);

router.delete('/:id', authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(authShipperRoleMiddleware),
    asyncWrapper(deleteLoadById),
);

router.get('/:id/shippin_info', authLoginMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(authShipperRoleMiddleware),
    asyncWrapper(getUserShippingInfo),
);

module.exports = router;
