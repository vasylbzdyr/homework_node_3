const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const PORT = process.env.PORT || 8080;
const app = express();

const authRouter = require('./router/authRouter');
const userRouter = require('./router/userRouter');
const truckRouter = require('./router/truckRouter');
const loadRouter = require('./router/loadRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

app.use((req, res) => {
  res.status(404).json({message: 'Page not found'});
});

/** Class representing a statusCode. */
class UnauthorizedError extends Error {
  /**
       * @param {string} message - The message value.
       */
  constructor(message = 'Unauthorized user') {
    super(message);
    statusCode = 400;
  }
}

app.use((err, req, res, next) => {
  if (err instanceof UnauthorizedError) {
    res.status(err.statusCode).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://vasyl:67K0fPoBCXF2Vm73@cluster0.qjhdg.mongodb.net/homework_node_3?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
  } catch (error) {
    res.status(500).json({message: error.message});
  }

  app.listen(PORT, () => {
    console.log(`Server works at port ${PORT}!`);
  });
};

start();
