const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');

module.exports.authLoginMiddleware = (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res
        .status(401)
        .json({message: 'No Authorization http header found!'});
  }

  const [, jwtToken] = header.split(' ');

  if (!jwtToken) {
    return res.status(401).json({message: 'No JWT token found'});
  }

  try {
    req.user = jwt.verify(jwtToken, JWT_SECRET);
    next();
  } catch (err) {
    return res.status(400).json({message: 'Bad token'});
  }
};

module.exports.authDriverRoleMiddleware = async (req, res, next) => {
  const {role} = await User.findById(req.user._id);

  if (role === 'DRIVER') {
    next();
  } else {
    return res
        .status(400)
        .json({message: `You aren't a driver! You don't have permission`});
  }
};

module.exports.authShipperRoleMiddleware = async (req, res, next) => {
  const {role} = await User.findById(req.user._id);

  if (role === 'SHIPPER') {
    next();
  } else {
    return res
        .status(400)
        .json({message: `You aren't a shipper! You don't have permission`});
  }
};
