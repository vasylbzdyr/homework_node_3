const {Truck} = require('../models/truckModel');

module.exports.checkAssignTruck = async (req, res, next) => {
  const assignTruck = await Truck.findOne({assigned_to: req.user._id});

  if (!assignTruck) {
    return res
        .status(400)
        .json({message: `You don't have any assigned truck`});
  } else {
    next();
  }
};
