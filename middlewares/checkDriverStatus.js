const {Truck} = require('../models/truckModel');

module.exports.checkDriverStatus = async (req, res, next) => {
  const busyTruck =
  await Truck.findOne({assigned_to: req.user._id, status: 'ON'});

  if (busyTruck) {
    return res
        .status(400)
        .json({
          message: `You can't cahnge any information' +
'while you are on load`});
  } else {
    next();
  }
};
