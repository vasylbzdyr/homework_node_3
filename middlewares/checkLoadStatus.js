const {Load} = require('../models/loadModel');

module.exports.checkLoadStatus = async (req, res, next) => {
  const {id} = req.params;
  const load = await Load.findById(id);

  if (load.status === 'ASSIGNED') {
    return res.status(400).json({message: `Load with id ${id} in way!`});
  }

  if (load.status === 'SHIPPED') {
    return res.status(400).json({message: `Load delivered`});
  }
  next();
};
