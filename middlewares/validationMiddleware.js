const Joi = require('joi');

module.exports.validateRegister = async (req, res, next) => {
  const schema = Joi.object({
    email: validateEmail(),
    password: validatePassword(),
    role: Joi.string().valid('SHIPPER', 'DRIVER').required(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateLogin = async (req, res, next) => {
  const schema = Joi.object({
    email: validateEmail(),
    password: validatePassword(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateEmail = async (req, res, next) => {
  const schema = Joi.object({
    email: validateEmail(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validatePassword = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: validatePassword(),
    newPassword: validatePassword(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateTruckType = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi
        .string()
        .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateLoad = async (req, res, next) => {
  const schema = Joi.object({
    name: validateString(),

    payload: Joi.number().required(),

    pickup_address: validateString(),

    delivery_address: validateString(),

    dimensions: {
      width: validateNumber(),
      length: validateNumber(),
      height: validateNumber(),
    },
  });

  await schema.validateAsync(req.body);
  next();
};

const validateEmail = () => {
  return Joi.string()
      .email({minDomainSegments: 2, tlds: {allow: ['com', 'net']}})
      .required();
};

const validatePassword = () => {
  return Joi.string()
      .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
      .required();
};

const validateString = () => {
  return Joi.string()
      .min(5)
      .required();
};

const validateNumber = () => {
  return Joi.number().required();
};
