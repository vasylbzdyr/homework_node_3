const {User} = require('../models/userModel');

module.exports.existingProfileMiddleware = async (req, res, next) => {
  const user = await User.findById(req.user._id);

  if (!user) {
    return res.status(404).json({message: `Didn't find this user`});
  }

  next();
};
